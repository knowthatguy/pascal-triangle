import pytest
from pascal_triangle import *


def test_pascal_triangle_empty():
    assert pascal_triangle() == []


def test_pascal_triangle():
    assert pascal_triangle(0) == []
    assert pascal_triangle(1) == [[1]]
    assert pascal_triangle(2) == [[1], [1, 1]]
    assert pascal_triangle(4) == [[1], [1, 1], [1, 2, 1], [1, 3, 3, 1]]


def test_pascal_triangle_error():
    with pytest.raises(AssertionError):
        pascal_triangle('abc')
