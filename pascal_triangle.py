# Printout the Pascal's Triangle
# Example:
# Input:
# 3

# Output
#    1
#   1 1
#  1 2 1

# d - depth

__all__ = ["pascal_triangle"]


def triangle(d, res=None):
    """
    Solution with recursion
    """
    if res is None:
        res = [[1]]
    if d == 1:
        return res
    else:
        prev_row = res[-1]
        new_row = [1] + [sum(i) for i in zip(prev_row, prev_row[1:])] + [1]
        return triangle(d - 1, res + [new_row])


def pascal_triangle(d: int = 0):
    """
    Returns the representation of Pascal's triangle as list of lists
    """
    assert type(d) == int
    result = [[1], [1, 1]]
    if d <= 2:
        return result[:d]

    else:
        for i in range(2, d):
            tmp = [1]
            previos_line = result[i-1]
            new_len = len(previos_line) + 1
            a, b = 0, 1
            while b < new_len-1:
                tmp.append(previos_line[a] + previos_line[b])
                a = b
                b += 1

            tmp.append(1)
            result.append(tmp)
    return result


def main():
    d = int(input())
    res = pascal_triangle(d)
    for i, v in enumerate(res):
        print(' '*(len(res) - i) + ' '.join(map(lambda x: str(x), v)))


if __name__ == "__main__":
    main()
